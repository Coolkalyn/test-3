
var MongoClient = require('mongodb').MongoClient,
    assert = require('assert');

// Connection URL
var url = 'mongodb://localhost:27017/test';
// Use connect method to connect to the Server
MongoClient.connect(url, function(err, db) {
  assert.equal(null, err);
  console.log("Connected correctly to server");
    // Insert multiple documents
    db.collection('contacts').insertMany([
        {
            'email': "happypeople.sleep@gmail.com",
            'password': "p1",
            'firstName': {'ru':"Давид",'en':"David"},
            'lastName': {'ru':"Аскаров",'en':"Askarov"},
            'position': "Programmer",
            'phoneNumber': "066-971-30-43",
            'birthday': "09.01.1990",
            'location': "Zaporizhzhya"
        },
        {
            'email': "ipawskal@gmail.com",
            'password': "parol1",
            'firstName': {'ru':"Павел",'en':"Pavel"},
            'lastName': {'ru':"Иванов",'en':"Ivanov"},
            'position': "Programmer",
            'phoneNumber': "067-107-42-02",
            'birthday': "22.06.1990",
            'location': "Zaporizhzhya"
        },
        {
            'email': "n.plakhtii.com",
            'password': "iithkalp",
            'firstName': {"en":"Nastasja", "ru": "Анастасия"},
            'lastName': {"en":"Plakhtii", "ru": "Плахтий" },
            'position': "Javascript developer",
            'phoneNumber': "066-81-55-704",
            'birthday': "19.04.1994",
            'location': "Zaporizhzhya"
        },
        {
            'email': "nikolaikornienko.job@gmail.com",
            'password': "parolparol",
            'firstName': {"en":"Nikolai", "ru": "Николай"},
            'lastName': {"en":"Kornienko", "ru": "Корниенко"},
            'position': "Programmer",
            'phoneNumber': "095-432-10-13",
            'birthday': "10.12.1992",
            'location': "Zaporizhzhya"
        },
        {
            'email':    "mrdach05@gmail.com",
            'password': "best-pass-ever",
            'firstName': {'ru':"Виктор",'en':"Viktor"},
            'lastName': {'ru':"Гончаров",'en':"Honcharov"},
            'position': "Programmer",
            'phoneNumber': "063-585-36-30",
            'birthday': "14.06.1996",
            'location': "Zaporizhzhya"
        },
        {
            'email': "zhenya.zhykov.1994@gmail.com",
            'password': "zhykov",
            'firstName': {'ru':"Евгений",'en':"Yevhen"},
            'lastName': {'ru':"Жуков",'en':"Zhukov"},
            'position': "Programmer",
            'phoneNumber': "066-64-18-734",
            'birthday': "10.02.1994",
            'location': "Zaporizhzhya"
        },
        {
            'email': "bliznyuk.sergei@gmail.com",
            'password': "wishmaster_1994",
            'firstName': {'ru':"Сергей",'en':"Serhey"},
            'lastName': {'ru':"Близнюк",'en':"Bliznyuk"},
            'position': "CTO",
            'phoneNumber': "050-542-23-43",
            'birthday': "16.03.1994",
            'location': "Zaporizhzhya",
            'admin': true
        },
        {
            'email':    "ellina.aleinikova@gmail.com",
            'password': "pantera0110",
            'firstName': {'ru':"Эллина",'en':"Ellina"},
            'lastName': {'ru':"Колесниченко",'en':"Kolisnichenko"},
            'position': "CTO (PHP)",
            'phoneNumber': "095-361-08-21",
            'birthday': "14.08.1993",
            'location': "Kyiv",
            'admin': true
        },
        {
            'email':    "polischuk.post@gmail.com",
            'password': "psl",
            'firstName': {'ru':"Сергей",'en':"Sergey"},
            'lastName': {'ru':"Полищук",'en':"Polischuk"},
            'position': "Programmer",
            'phoneNumber': "067-878-48-63",
            'birthday': "07.08.1985",
            'location': "Kyiv"
        }
    ], function(err, r) {
      assert.equal(null, err);
      assert.equal(7, r.insertedCount);
      console.log("inserted files!");
      db.close();
  });
});
