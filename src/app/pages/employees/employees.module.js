(function () {
  'use strict';

  angular.module('BlurAdmin.pages.employees', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('employees', {
          url: '/employees',
          title: 'Employees',
          templateUrl: 'app/pages/employees/employees.html',
          controller: 'EmployeesPageCtrl',
          resolve: {
              contacts: function(ContactsPageService) {
                  return ContactsPageService.getContacts();
              }
          },
          sidebarMeta: {
            icon: 'ion-grid',
            order: 1,
          },
        });
  }

})();
