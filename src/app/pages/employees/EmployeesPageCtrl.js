(function() {
    'use strict';

    angular.module('BlurAdmin.pages.employees')
        .controller('EmployeesPageCtrl', EmployeesPageCtrl);

    /** @ngInject */
    function EmployeesPageCtrl(contacts, $scope, $location, $sessionStorage, editableOptions, editableThemes, ContactsPageService, $rootScope, $injector, SessionService) {
        //show contacts
        $scope.contacts = contacts.data;

        $scope.$sessionStorage = $injector.get('$sessionStorage');

        //pagination
        $scope.contactsPageSize = 5;

        $scope.check = false;

        //Add new contact
        $scope.addUser = function() {
            $scope.inserted = {
                'email': "empty@com",
                'password': "",
                'firstName': "no",
                'lastName': "name",
                'position': "",
                'salary': "",
                'phoneNumber': "",
                'birthday': "",
                'Info': ""
            };
            $scope.contacts.push($scope.inserted);
        }


        //delete contact
        $scope.removeUser = function(contactId) {
            if (contactId !== undefined) {
                ContactsPageService.deleteContact(contactId);
            }
            for (var i in $scope.contacts) {
                if ($scope.contacts[i]._id == contactId) {
                    $scope.contacts.splice(i, 1);
                }
            }
        };

        //editable
        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';


        $scope.findUser = function(profileId) {
          if ($scope.check == false) {
          var url = "/profile/" + profileId;
                $location.path(url);
              }
        }


        //cancelNewUser
        $scope.cancelUser = function(contact) {
          if (contact._id == undefined) {
            $scope.removeUser(contact._id);
          }
        }


        //savingChanges
        $scope.saveUser = function(contact, data) {
            if (data.firstName !== undefined && data.lastName !== undefined && data.email !== undefined && data.firstName !== "" && data.lastName !== "" && data.email !== "") {
                if (contact._id !== undefined && contact._id !== "") {
                  for (var i in data) {
                      contact[i] = data[i];
                  }
                  ContactsPageService.editContact(contact);
                } else {
                    ContactsPageService.createContact(data).then(function(doc) {
                        $scope.contacts.push(doc.data);
                        $scope.removeUser(contact._id);
                    }, function(response) {
                        alert(response);
                    });
                }
            } else {
              alert("Заполните все недостающие поля");
              throw new Error("stop");
            }
        }

        //CheckforAdmin
        $scope.isAdmin = function() {
            if ($scope.$sessionStorage.user !== undefined) {
                if ($scope.$sessionStorage.user.admin) {
                  $scope.contactsPageSize = 100;
                    return true;
                } else {
                    return false;
                }
            }
        }

    }

})();
