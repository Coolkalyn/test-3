/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.pages.profile')
    .controller('ProfilePageCtrl', ProfilePageCtrl);

  /** @ngInject */
  function ProfilePageCtrl($scope, fileReader, $filter, $uibModal, $rootScope, $injector, ContactsPageService, $stateParams) {
    $scope.$sessionStorage = $injector.get('$sessionStorage');
    var Id = $stateParams.profileId;
    ContactsPageService.getContact(Id).then(function(doc) {
        $scope.user = doc.data;
        if($scope.user.profilePic){
            $scope.picture = $filter('profilePicture')($scope.user.profilePic);
        } else {
            $scope.picture = $filter('appImage')('theme/no-photo.png');
        }
    }, function(response) {
        alert(response);
    });

    //getCurrentUser
    $scope.getCurrentUser = function() {
        return $scope.$sessionStorage.user
    }

    //savingChanges
    $scope.Update = function(contact, oldPass, newPass) {
        if (contact.firstName !== undefined && contact.lastName !== undefined && contact.email !== undefined && contact.firstName !== "" && contact.lastName !== "" && contact.email !== "") {
            if (oldPass !== undefined && oldPass !== "") {
              if (oldPass == $scope.user.password) {
                contact.password = newPass;
                ContactsPageService.editContact(contact);
                alert("Профиль обновлён с паролем");
                      $scope.$state.reload()

              } else {
                alert("Введите верно старый пароль");
              }
            } else {
              ContactsPageService.editContact(contact);
              alert("Профиль обновлён");
                    $scope.$state.reload()
            }
          } else {
            alert("Введите имя, фамилию и почту");
          }
    }

    $scope.removePicture = function () {
      $scope.picture = $filter('appImage')('theme/no-photo.png');
      $scope.noPicture = true;
    };

    $scope.uploadPicture = function () {
      var fileInput = document.getElementById('uploadFile');
      fileInput.click();

    };

    $scope.socialProfiles = [
      {
        name: 'Facebook',
        href: 'https://www.facebook.com/akveo/',
        icon: 'socicon-facebook'
      },
      {
        name: 'Twitter',
        href: 'https://twitter.com/akveo_inc',
        icon: 'socicon-twitter'
      },
      {
        name: 'Google',
        icon: 'socicon-google'
      },
      {
        name: 'LinkedIn',
        href: 'https://www.linkedin.com/company/akveo',
        icon: 'socicon-linkedin'
      },
      {
        name: 'GitHub',
        href: 'https://github.com/akveo',
        icon: 'socicon-github'
      },
      {
        name: 'StackOverflow',
        icon: 'socicon-stackoverflow'
      },
      {
        name: 'Dribbble',
        icon: 'socicon-dribble'
      },
      {
        name: 'Behance',
        icon: 'socicon-behace'
      }
    ];

    $scope.unconnect = function (item) {
      item.href = undefined;
    };

    $scope.showModal = function (item) {
      if ($scope.getCurrentUser()._id == $scope.user._id) {
      $uibModal.open({
        animation: false,
        controller: 'ProfileModalCtrl',
        templateUrl: 'app/pages/profile/profileModal.html'
      }).result.then(function (link) {
          item.href = link;
        });
      }
    };

    $scope.getFile = function (file) {
      fileReader.readAsDataUrl(file, $scope)
          .then(function (result) {
            $scope.picture = result;
          });
    };

    $scope.switches = [true, true, false, true, true, false];
  }

})();
