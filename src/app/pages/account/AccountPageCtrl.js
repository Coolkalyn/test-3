(function() {
    'use strict';

    angular.module('BlurAdmin.pages.account')
        .controller('AccountPageCtrl', AccountPageCtrl);

    /** @ngInject */
    function AccountPageCtrl(contacts, $scope, $location, $sessionStorage, editableOptions, editableThemes, ContactsPageService, $rootScope, $injector, SessionService) {
        //show contacts
        $scope.contacts = contacts.data;

    $scope.$sessionStorage = $injector.get('$sessionStorage');


    //Login
    $scope.login = function(user) {
      if ($scope.isLoggedIn()) {
        alert("Сначала выйдите с текущего профиля");
      } else {
        $scope.log = "";
        for (var i in $scope.contacts) {
            if ($scope.contacts[i].email == user.email) {
                $scope.log = $scope.contacts[i];
            }
        }
        if ($scope.log) {
            if ($scope.log.password == user.password) {
                $scope.$sessionStorage.user = $scope.log;
                $location.path("/employees");
            } else {
                alert("Вы ввели неправильный пароль");
            }
        } else {
            alert("Вы ввели неправильный email");
        }
      }
    }

    //Registration
    $scope.reg = function(contact) {
      if ($scope.isLoggedIn()) {
        alert("Сначала выйдите с текущего профиля");
      } else {
        var k = 0;
        for (var i in $scope.contacts) {
            if ($scope.contacts[i].email == contact.email) {
                k++;
            }
        }
        if (k == 0) {
            if (contact.firstName !== undefined && contact.lastName !== undefined && contact.email !== undefined && contact.password !== undefined && contact.email !== "") {
                ContactsPageService.createContact(contact).then(function(doc) {
                    $scope.contacts.push(doc.data);
                    var id = doc.data._id;
                    alert("Вы зарегистрировались!");
                    $scope.newUser = "";
                    $scope.$sessionStorage.user = doc.data;
                    var url = "/profile/" + id;
                    $location.path(url);
                }, function(response) {
                    alert(response);
                });
            } else {
                alert("Заполните все недостающие поля");
            }
        } else {
            alert("Такой email уже существует");
        }
      }
    }


    //checkForUser
    $scope.isLoggedIn = function() {
        if ($scope.$sessionStorage.user !== undefined) {
            return true;
        } else {
            return false;
        }
    }



}


})();
