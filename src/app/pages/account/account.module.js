(function() {
    'use strict';

    angular.module('BlurAdmin.pages.account', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('signup', {
                url: '/signup',
                title: 'Sign Up',
                templateUrl: 'app/pages/account/reg.html',
                controller: 'AccountPageCtrl',
                data: {
                    'noLogin': true
                  },
                  resolve: {
                      contacts: function(ContactsPageService) {
                          return ContactsPageService.getContacts();
                      }
                  },
            })
            .state('login', {
                url: '/login',
                title: 'Log In',
                templateUrl: 'app/pages/account/auth.html',
                controller: 'AccountPageCtrl',
                data: {
                    'noLogin': true
                  },
                  resolve: {
                      contacts: function(ContactsPageService) {
                          return ContactsPageService.getContacts();
                      }
                  },
            });
    }

})();
