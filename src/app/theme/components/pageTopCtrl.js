(function() {
        'use strict';

        angular.module('BlurAdmin.theme.components')
            .controller('pageTopCtrl', pageTopCtrl);

        /** @ngInject */
        function pageTopCtrl($scope, $location, $sessionStorage, ContactsPageService, $injector, SessionService) {

            $scope.$sessionStorage = $injector.get('$sessionStorage');

            $scope.logOut = function() {
                $scope.$sessionStorage.user = undefined;
                $location.path("/login");
            }

            //checkForUser
            $scope.isLoggedIn = function() {
                if ($scope.$sessionStorage.user !== undefined) {
                    return true;
                } else {
                    return false;
                }
            }

            //getCurrentUser
            $scope.getCurrentUser = function() {
                return $scope.$sessionStorage.user
            }

            $scope.profile = function() {
              if ($scope.getCurrentUser()._id !== undefined) {
              var profileId = $scope.getCurrentUser()._id;
              var url = "/profile/" + profileId;
                    $location.path(url);
                  }
            }

            //CheckforAdmin
            $scope.isAdmin = function() {
                if ($scope.$sessionStorage.user !== undefined) {
                    if ($scope.$sessionStorage.user.admin) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }  


        }

})();
